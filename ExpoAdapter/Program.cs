﻿using System;

namespace ExpoAdapter
{
    class Program
    {
        static void Main(string[] args)
        { 
            // Cliente
            int resultado = 0;

        // Se utiliza una interfaz y clase que el cliente conozca
        IObjetivo calculadora = new CCalculadora();
        resultado = calculadora.Sumar(4, 9);
        Console.WriteLine("El resultado de la suma es {0}", resultado);
        Console.WriteLine("*****");
        //Utilizamos el Adapter
        calculadora = new CAdapter();
        //Utilizamos el Adapter para realizar la operación
        resultado = calculadora.Sumar(8, 3);
        Console.WriteLine("El resultado es {0}", resultado);
        
            

        }
    }
}
