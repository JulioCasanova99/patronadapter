﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpoAdapter
{
    //Este es el adaptador, utiliza la interfaz IObjetivo
    //El Adapter comunica al cliente con la clase adaptada 
    class CAdapter:IObjetivo
    {
        CCalculo adaptador = new CCalculo();
        //Se adapta la operación
        public int Sumar(int V1, int V2)
        {
            double r = 0;
            //Adaptamos los datos que se envían
            int[] operador = { V1, V2 };
            //Operacion en el adaptado
            r = adaptador.suma(operador);
            //Se adapta el resultado
            return (int)r;
        }


    }
}
