﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpoAdapter
{
    //Clase a adaptar pero el cliente no podra usar de forma
    //directa ya que posee una interfaz diferente
    class CCalculo
    {
        public double suma(int[] Operacion)
        {
            int n = 0;
            double r = 0;
            for (n = 0; n < Operacion.Length; n++)
                r += Operacion[n];
            return r;
        }
    }
}
