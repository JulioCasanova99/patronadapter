﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExpoAdapter
{
   // Interfaz que el cliente ya conoce
    interface IObjetivo
    {
        int Sumar(int V1, int V2);
    }
}
